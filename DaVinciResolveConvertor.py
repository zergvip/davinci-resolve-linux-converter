import os
import uuid
import sys

directoryin = sys.argv[1]
directoryout = sys.argv[2]

files = os.listdir(directoryin)

for k in files:
	filename, file_extension = os.path.splitext(k)
	file_extension = file_extension.lower()	
	if file_extension == ".mov" :		
		os.system("ffmpeg -i \"" + directoryin + "/" + k + "\" -vcodec mjpeg -q:v 1 -acodec pcm_s16be -q:a 0 -f mov \"" + directoryout + "/" +  filename + ".mov\"")
    
